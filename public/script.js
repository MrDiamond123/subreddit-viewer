//Thanks StackOverFlow (https://stackoverflow.com/a/2648463)
String.prototype.format = String.prototype.f = function() {
    var s = this,
        i = arguments.length;

    while (i--) {
        s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]);
    }
    return s;
};

var template;
$.get('template.html', function(data) {
    template = data;
});

var gsubreddit;
var after;

function getAndDisplaySubreddit (subreddit) {
    gsubreddit = subreddit;
    $(".body").empty();
    $.getJSON( "https://www.reddit.com/r/" + subreddit + "/search.json?q=self:0&sort=top&t=day&restrict_sr=on", function(data) {
    after = data["data"]["after"];
    var items = [];
    $.each(data["data"]["children"], function (key, val) {
        var url = val["data"]["url"];
       // if (!(url.endsWith(".jpg") || url.endsWith(".png"))) {
        //    var url = val["data"]["url"] + ".jpg"
        //}
        if (url.includes("imgur") & !(url.includes("i.imgur")) ) {
            var url = val["data"]["url"] + ".gif"
        }
        items.push(template.f(
            url, val["data"]["title"], "https://www.reddit.com" + val["data"]["permalink"], "https://www.reddit.com/u/" + val["data"]["author"], val["data"]["author"]
        ));
        // items.push("<li class = 'media'> " + 
        // "<a href='" + val["data"]["url"] + "'>" + 
        // "<img src='" + val["data"]["thumbnail"] + "'class='img-thumbnail mr-3'>" + "<div class = 'media-body'> " + 
        // "<h5 class='m-auto'>" + val["data"]["title"] + "</h5> </a> </div> </li>");
    })
    $( "<div/>", {
       // "class": "list-unstyled",
        "class": "card-columns",
        html: items.join( "" )
  }).appendTo( "div" );
});
}
function displayMorePosts() {
    $.getJSON( "https://www.reddit.com/r/" + gsubreddit + "/search.json?q=self:0&sort=top&t=day&restrict_sr=on&after=" + after, function(data) {
    after = data["data"]["after"];
    var items = [];
    $.each(data["data"]["children"], function (key, val) {
        var url = val["data"]["url"];
       // if (!(url.endsWith(".jpg") || url.endsWith(".png"))) {
        //    var url = val["data"]["url"] + ".jpg"
        //}
        if (url.includes("imgur") & !(url.includes("i.imgur")) ) {
            var url = val["data"]["url"] + ".gif"
        }
        items.push(template.f(
            url, val["data"]["title"], "https://www.reddit.com" + val["data"]["permalink"], "https://www.reddit.com/u/" + val["data"]["author"], val["data"]["author"]
        ));
        // items.push("<li class = 'media'> " + 
        // "<a href='" + val["data"]["url"] + "'>" + 
        // "<img src='" + val["data"]["thumbnail"] + "'class='img-thumbnail mr-3'>" + "<div class = 'media-body'> " + 
        // "<h5 class='m-auto'>" + val["data"]["title"] + "</h5> </a> </div> </li>");
    })
    $( "<div/>", {
       // "class": "list-unstyled",
        "class": "card-columns",
        html: items.join( "" )
  }).appendTo( "div" );
});
}

getAndDisplaySubreddit("earthporn");